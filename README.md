# Spotify AutoPlaylists Server

A fork of the original spotify playlists app. This contains all of the server side code. It runs a graphql server that connects to a postgres database to manage sessions, as well as runs recurring jobs to update playlists.

This fork maintains most of the original logic, but migrates to postgres and updates to use typescript.

Available [here](http://spotifyapps.chriswbarry.com/ "SpotifyApps")

This project uses postgres

## ./server

### ./routes

Handle the API

## ./server/Playlists

### mostPlayed.js

Updates a Spotify playlist based on the most played Last.FM tracks

### recentlyAdded.js

Updates a Spotify playlist based on recently added Spotify tracks

### Playlist.js

The playlist parent class, contains everything the app needs for making/editing/clearing/updating spotify playlists

## Config

The app is configured though environment variables. A full list is visible in typings/index.d.ts
