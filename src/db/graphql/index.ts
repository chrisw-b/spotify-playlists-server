import { GraphQLSchema } from "graphql/type";
import mutation from "~/db/graphql/mutations";
import query from "~/db/graphql/queries";

export default new GraphQLSchema({ mutation, query });
