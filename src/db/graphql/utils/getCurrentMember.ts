import type { Profile } from "passport-spotify";
import validMember from "~/db/graphql/utils/validMember";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";

export const getCurrentMember = async (
  user: Profile | undefined,
  spotifyId: string | undefined,
) => {
  // default to current member if none is provided
  const id = spotifyId ?? user?.id;

  // block access of other accounts
  if (!validMember(user, id)) {
    throw new Error("Incorrect permissions");
  }

  const memberRepo = DB.getRepository(Member);

  const queriedMember = await memberRepo.findOne({
    where: { spotifyId: id ?? "" },
    relations: ["recentlyAdded", "mostPlayed"],
  });

  return queriedMember;
};
