import type { Profile } from "passport-spotify";

const validMember = (user: Profile | undefined, spotifyId = "") =>
  !!(user?.id === spotifyId || user?.id === process.env.ADMIN);

export default validMember;
