import type { Request } from "express";
import {
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";
import type { Period } from "lastfm-njs";
import type { Profile } from "passport-spotify";
import backendActionType from "~/db/graphql/types/backendActionType";
import memberType from "~/db/graphql/types/memberType";
import updatePlaylistType from "~/db/graphql/types/updatePlaylistType";
import { getCurrentMember } from "~/db/graphql/utils/getCurrentMember";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";

type PatchArgs = {
  spotifyId: string;
  playlistKind: "most" | "recent";
  patch: {
    length?: number;
    period?: Period;
    lastfm?: string;
    enabled?: boolean;
  };
};

const mutations = new GraphQLObjectType<undefined, Request>({
  name: "RootMutationType",
  description: "Everything we can change",
  fields: {
    updatePlaylist: {
      type: memberType,
      description: "updates a playlist object",
      args: {
        spotifyId: {
          description: "The current member's id",
          type: GraphQLString,
        },
        playlistKind: {
          description:
            "The kind of playlist, either mostPlayed or recentlyAdded",
          type: new GraphQLEnumType({
            name: "playlistType",
            values: {
              mostPlayed: { value: "most" },
              recentlyAdded: { value: "recent" },
            },
          }),
        },
        patch: {
          description: "Everything we want to change",
          type: new GraphQLNonNull(updatePlaylistType),
        },
      },
      resolve: async (_, args: PatchArgs, req) => {
        const { playlistKind, patch, spotifyId } = args;
        const user = req?.user as Profile;

        const foundItem = await getCurrentMember(user, spotifyId);

        const safePatch = { ...patch };

        if (playlistKind === "most" && foundItem) {
          foundItem.mostPlayed = { ...foundItem.mostPlayed, ...safePatch };
        } else if (playlistKind === "recent" && foundItem) {
          safePatch.period = undefined;
          safePatch.lastfm = undefined;
          foundItem.recentlyAdded = {
            ...foundItem.recentlyAdded,
            ...safePatch,
          };
        }
        if (foundItem) {
          const memberRepo = DB.getRepository(Member);
          await memberRepo.save(foundItem);
        }
        return foundItem;
      },
    },
    deleteAccount: {
      type: backendActionType,
      description: "removes a member and logs them out",
      resolve: async (_, __, req) => {
        if (!req.user?.id) {
          return { success: false };
        }

        const memberRepo = DB.getRepository(Member);
        const memberToRemove = await memberRepo.findOneBy({
          spotifyId: req.user.id,
        });
        if (memberToRemove) {
          await memberRepo.remove(memberToRemove);
        }
        req.logout({ keepSessionInfo: false }, () => null);
        req.session.destroy(() => null);
        return { success: true };
      },
    },
    logout: {
      type: backendActionType,
      description: "logs a person out",
      resolve: (_, __, req) => {
        if (!req.user?.id) {
          return { success: false };
        }
        req.logout({ keepSessionInfo: false }, () => null);
        req.session.destroy(() => null);
        return { success: true };
      },
    },
  },
});

export default mutations;
