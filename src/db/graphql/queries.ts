import type { Request } from "express";
import { GraphQLList, GraphQLObjectType, GraphQLString } from "graphql/type";
import type { Profile } from "passport-spotify";
import memberType from "~/db/graphql/types/memberType";
import { getCurrentMember } from "~/db/graphql/utils/getCurrentMember";
import validMember from "~/db/graphql/utils/validMember";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";

type MemberArgs = {
  spotifyId?: string;
};

const queries = new GraphQLObjectType<undefined, Request>({
  name: "RootQueryType",
  description: "Everything we can look up",
  fields: () => ({
    member: {
      type: memberType,
      args: { spotifyId: { name: "spotifyId", type: GraphQLString } },
      description: "A single members info",
      resolve: async (_, args: MemberArgs, req) => {
        const user = req?.user as Profile;
        const { spotifyId } = args;

        const currentMember = await getCurrentMember(user, spotifyId);

        return currentMember;
      },
    },
    members: {
      type: new GraphQLList(memberType),
      resolve: async (_, __, req) => {
        const user = req?.user as Profile;
        // don't allow non admins to view full member list
        if (!validMember(user, process.env.ADMIN)) {
          throw new Error("Incorrect permissions");
        }
        const memberRepo = DB.getRepository(Member);
        return await memberRepo.find();
      },
    },
  }),
});

export default queries;
