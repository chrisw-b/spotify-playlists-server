import validMember from "~/db/graphql/utils/validMember";

describe("Backend Action Type", () => {
  process.env.NODE_ENV = "development";
  it("should register as valid member if in dev mode", () => {
    // @ts-expect-error just for testing
    expect(validMember({ id: "bob" }, "rob")).toBe(true);
  });
  process.env.NODE_ENV = "production";
  process.env.ADMIN = "bob";
  it("should register as valid member if is admin", () => {
    // @ts-expect-error just for testing
    expect(validMember({ id: "bob" }, "rob")).toBe(true);
  });

  it("should register as valid member if accessing self", () => {
    // @ts-expect-error just for testing
    expect(validMember({ id: "rob" }, "rob")).toBe(true);
  });

  it("should register as invalid member if trying to access someone else in production", () => {
    // @ts-expect-error just for testing
    expect(validMember({ id: "rob" }, "bob")).toBe(false);
  });

  it("should register as invalid member if trying to access someone else in production while not logged in", () => {
    // @ts-expect-error just for testing
    expect(validMember({}, "bob")).toBe(false);
  });

  it("should allow admin in without a spotify id", () => {
    // @ts-expect-error just for testing
    expect(validMember({ id: "bob" })).toBe(true);
  });

  it("should block non-admins without a spotify id", () => {
    // @ts-expect-error just for testing
    expect(validMember({ id: "rob" })).toBe(false);
  });

  it("should block non-logged in people without a spotify id", () => {
    // @ts-expect-error just for testing
    expect(validMember({})).toBe(false);
  });
});
