import type { Request } from "express";
import {
  GraphQLBoolean,
  GraphQLInt,
  GraphQLObjectType,
  GraphQLString,
} from "graphql/type";
import mostPlayedType from "~/db/graphql/types/mostPlayedType";
import recentlyAddedType from "~/db/graphql/types/recentlyAddedType";
import type { Member } from "~/db/postgres/schema/Member";

const memberType = new GraphQLObjectType<Member, Request>({
  name: "member",
  description: "a member",
  fields: () => ({
    spotifyId: {
      type: GraphQLString,
      description: "The spotify id of the member",
    },
    photo: {
      type: GraphQLString,
      description: "a photo of the member",
    },
    isAdmin: {
      type: GraphQLBoolean,
      description: "whether the member is an admin",
    },
    visits: {
      type: GraphQLInt,
      description: "The number of times a person has visited",
    },
    mostPlayed: {
      type: mostPlayedType,
      description: "The member's most played playlist info",
    },
    recentlyAdded: {
      type: recentlyAddedType,
      description: "The member's recently added playlist info",
    },
  }),
});

export default memberType;
