import { GraphQLBoolean } from "graphql/type";
import backendActionType from "~/db/graphql/types/backendActionType";

describe("Backend Action Type", () => {
  it("should have a success field", () => {
    expect(backendActionType.getFields()).toHaveProperty("success");
  });

  it("success field should be of type boolean", () => {
    expect(backendActionType.getFields().success?.type).toEqual(GraphQLBoolean);
  });
});
