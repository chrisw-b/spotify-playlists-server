import { DataSource } from "typeorm";
import { Member } from "~/db/postgres/schema/Member";
import { MostPlayedPlaylist, Playlist } from "~/db/postgres/schema/Playlist";

const AppDataSource = new DataSource({
  type: "postgres",
  synchronize: true,
  url: process.env.DB_CONNECTION_STRING,
  entities: [Member, Playlist, MostPlayedPlaylist],
});

export default AppDataSource;
