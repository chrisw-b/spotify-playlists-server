import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
  type Relation,
} from "typeorm";
import { MostPlayedPlaylist, Playlist } from "~/db/postgres/schema/Playlist";

@Entity()
export class Member {
  @PrimaryColumn({ type: "text", unique: true, update: false })
  spotifyId!: string;

  @Column({ type: "int", default: 0, nullable: false })
  visits!: number;

  @Column({ type: "boolean", default: false, nullable: false })
  isAdmin!: boolean;

  @Column({ type: "text", nullable: true })
  photo?: string;

  @Column("text")
  refreshToken!: string;

  @Column("text")
  accessToken!: string;

  @OneToOne(() => MostPlayedPlaylist, { cascade: true })
  @JoinColumn()
  mostPlayed!: Relation<MostPlayedPlaylist>;

  @OneToOne(() => Playlist, { cascade: true })
  @JoinColumn()
  recentlyAdded!: Relation<Playlist>;
}
