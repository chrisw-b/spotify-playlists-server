import { Period } from "lastfm-njs";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Playlist {
  @PrimaryGeneratedColumn("uuid")
  uid!: string;
  @Column({ type: "text", nullable: true })
  playlistId?: string | undefined;
  @Column({ type: "int", default: 10, nullable: false })
  length!: number;
  @Column({ type: "boolean", default: false, nullable: false })
  enabled!: boolean;
}

@Entity()
export class MostPlayedPlaylist extends Playlist {
  @Column({ type: "enum", enum: Period, default: Period.WEEK, nullable: false })
  period!: Period;
  @Column({ type: "text", nullable: true })
  lastfm?: string;
}
