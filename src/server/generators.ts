import MostPlayed from "~/server/Playlists/mostPlayed";
import RecentlyAdded from "~/server/Playlists/recentlyAdded";
import { lastfmConfig, spotifyConfig } from "~/server/configs";

let mostPlayed: MostPlayed | undefined;
let recentlyAdded: RecentlyAdded | undefined;

export const getMostPlayed = () => {
  if (!mostPlayed) {
    mostPlayed = new MostPlayed(spotifyConfig, lastfmConfig);
  }
  return mostPlayed;
};

export const getRecentlyAdded = () => {
  if (!recentlyAdded) {
    recentlyAdded = new RecentlyAdded(spotifyConfig);
  }
  return recentlyAdded;
};
