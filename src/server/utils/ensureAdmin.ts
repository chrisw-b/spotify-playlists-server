import type { RequestHandler } from "express-serve-static-core";

const ensureAdmin: RequestHandler = (req, res, next) => {
  if (req.user?.id === process.env.ADMIN) {
    next();
  } else {
    res.sendStatus(403);
  }
};

export default ensureAdmin;
