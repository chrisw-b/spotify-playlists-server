import type { RequestHandler } from "express-serve-static-core";

const ensureAuthenticated: RequestHandler = (req, res, next) => {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.sendStatus(401);
  }
};

export default ensureAuthenticated;
