import type { Period, UserGetTopTracksRes } from "lastfm-njs";
import Lastfm from "lastfm-njs";
import sleep from "sleep-promise";
import type SpotifyWebApi from "spotify-web-api-node";
import { ONE_MIN, ONE_SEC } from "~/constants";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";
import Playlist from "~/server/Playlists/Playlist";

type SpotifyTrack = {
  id: string;
  rank: string;
};

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}

export default class MostPlayed extends Playlist {
  lastfm: Lastfm;
  constructor(
    spotifyData: ConstructorParameters<typeof SpotifyWebApi>[0],
    lastFmData: ConstructorParameters<typeof Lastfm>[0],
  ) {
    super(spotifyData);
    this.lastfm = new Lastfm(lastFmData);
    this.playlistName = "Most Played";
  }

  override isEnabled(member: Member) {
    this.log(
      `${member.spotifyId} ${this.playlistName ?? ""} is ${
        member.mostPlayed.enabled ? "en" : "dis"
      }abled`,
    );
    return member.mostPlayed.enabled;
  }

  // takes list of last.fm tracks and tries to find them in spotify
  async convertToSpotify(
    topTracks: UserGetTopTracksRes["track"],
  ): Promise<(SpotifyTrack | undefined)[]> {
    return await Promise.all(
      topTracks.map(async (ele, i) => {
        await sleep((ONE_SEC / 2) * i);
        const cleanArtist = ele.artist.name.replace(/['()]/g, "");
        const cleanTrack = ele.name.replace(/['()]/g, "");
        this.log(`Searching for \n${cleanTrack} by ${cleanArtist}`);
        const spotifyQuery = await this.spotifyApi.searchTracks(
          `track:${cleanTrack} artist:${cleanArtist}`,
        );
        const results = spotifyQuery.body.tracks?.items;

        if (results && results.length > 0 && results[0]?.uri) {
          return {
            id: results[0].uri,
            rank: ele["@attr"].rank,
          };
        }
        this.log(`couldn't find ${cleanTrack} by ${cleanArtist}`);
        return undefined;
      }),
    );
  }

  async insertMissingTracks(
    trackList: (SpotifyTrack | undefined)[],
    lastFmId: string,
    period: Period,
  ): Promise<(SpotifyTrack | null)[] | null> {
    let nextTrackSet: (SpotifyTrack | undefined)[] | undefined;
    return await Promise.all(
      trackList.map(async (ele) => {
        if (ele !== undefined) {
          return new Promise((resolve) => {
            resolve(ele);
          });
        }
        if (nextTrackSet === undefined) {
          const topTracks = await this.lastfm.user_getTopTracks({
            user: lastFmId,
            limit: trackList.length,
            period,
            page: 2,
          });
          if (!topTracks.success) {
            throw new Error(`Error: ${topTracks.error.toString()}`);
          }
          const converted = await this.convertToSpotify(topTracks.track);
          nextTrackSet = [];
          converted.forEach((track, i) => {
            (nextTrackSet as (SpotifyTrack | undefined)[])[i] = undefined;
            if (track !== undefined) {
              new Promise((resolve) => {
                resolve(track);
              }).catch((reason: unknown) => {
                throw new Error(
                  `Error getting track: ${JSON.stringify(reason)}`,
                );
              });
            }
          });
        } else {
          nextTrackSet.forEach((track, i) => {
            (nextTrackSet as (SpotifyTrack | undefined)[])[i] = undefined;
            if (track !== undefined) {
              new Promise((resolve) => {
                resolve(track);
              }).catch((reason: unknown) => {
                throw new Error(
                  `Error getting track: ${JSON.stringify(reason)}`,
                );
              });
            }
          });
        }
        return null;
      }),
    );
  }

  override async updatePlaylist(member: Member, delayInc = 0) {
    const newMember = member;
    await sleep(delayInc * ONE_MIN * 5);
    const { length, lastfm, period, playlistId } = member.mostPlayed;
    if (!(length && lastfm)) {
      return;
    }

    this.log("Logging in to Spotify");
    if (!(member.accessToken && member.refreshToken)) {
      throw new Error(
        "Member has no access or refresh token set, cannot refresh",
      );
    }
    const accessToken = member.accessToken;
    const refreshToken = member.refreshToken;

    const { accessToken: updatedAccess, refreshToken: updatedRefresh } =
      await this.signInToSpotify(accessToken, refreshToken);
    newMember.accessToken = updatedAccess;
    newMember.refreshToken = updatedRefresh;

    this.log("Logging in to LastFM");
    await this.lastfm.auth_getMobileSession();

    this.log("getting last.fm top tracks");
    const lastFmTrackList = await this.lastfm.user_getTopTracks({
      user: lastfm,
      limit: +length,
      period,
    });
    if (!lastFmTrackList.success) {
      throw new Error(`Error: ${lastFmTrackList.error.toString()}`);
    }

    this.log("converting to spotify and getting userinfo");
    const spotifyList = await this.convertToSpotify(lastFmTrackList.track);
    const spotifyUser = await this.spotifyApi.getMe();
    const spotifyId = spotifyUser.body.id;

    const trackList = await this.insertMissingTracks(
      spotifyList,
      lastfm,
      period,
    );

    this.log("sorting tracks, getting user, and preparing playlist");
    newMember.mostPlayed.playlistId = await this.preparePlaylist(
      spotifyId,
      playlistId,
    );
    const sortedTracks = (trackList ?? [])
      .filter(notEmpty)
      .sort((a, b) => +a.rank - +b.rank);
    this.log("filling playlist");
    await this.fillPlaylist(newMember.mostPlayed.playlistId, sortedTracks);
    const memberRepo = DB.getRepository(Member);

    this.log("saving member");
    await memberRepo.save(newMember);
  }
}
