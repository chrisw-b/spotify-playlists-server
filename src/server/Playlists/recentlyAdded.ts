import sleep from "sleep-promise";
import type SpotifyWebApi from "spotify-web-api-node";
import { ONE_MIN } from "~/constants";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";
import Playlist from "~/server/Playlists/Playlist";

export default class RecentlyAdded extends Playlist {
  constructor(spotifyData: ConstructorParameters<typeof SpotifyWebApi>[0]) {
    super(spotifyData);
    this.playlistName = "Recently Added";
  }

  override isEnabled(member: Member) {
    this.log(
      `${member.spotifyId} ${this.playlistName ?? ""} is ${
        member.recentlyAdded.enabled ? "en" : "dis"
      }abled`,
    );
    return member.recentlyAdded.enabled;
  }

  createTrackListArray(
    tracks: Awaited<
      ReturnType<SpotifyWebApi["getMySavedTracks"]>
    >["body"]["items"],
  ) {
    this.log("mapping tracks");
    return tracks.map((t) => t.track.uri);
  }

  override async updatePlaylist(member: Member, delayInc = 0) {
    const newMember = member;
    await sleep(delayInc * ONE_MIN * 5);
    const { playlistId, length } = member.recentlyAdded;

    if (!length) {
      return;
    }

    this.log("Logging in to spotify");
    if (!(member.accessToken && member.refreshToken)) {
      throw new Error(
        "Member has no access or refresh token set, cannot refresh",
      );
    }
    const accessToken = member.accessToken;
    const refreshToken = member.refreshToken;

    const { accessToken: updatedAccess, refreshToken: updatedRefresh } =
      await this.signInToSpotify(accessToken, refreshToken);
    newMember.accessToken = updatedAccess;
    newMember.refreshToken = updatedRefresh;

    this.log("Getting user info");
    const {
      body: { id: spotifyId },
    } = await this.spotifyApi.getMe();

    this.log("preparing playlist and getting saved tracks");
    newMember.recentlyAdded.playlistId = await this.preparePlaylist(
      spotifyId,
      playlistId,
    );
    const savedTracks = (
      await this.spotifyApi.getMySavedTracks({ limit: length })
    ).body;

    this.log("filling playlist");
    const spotifyUris = this.createTrackListArray(savedTracks.items);
    await this.spotifyApi.addTracksToPlaylist(
      newMember.recentlyAdded.playlistId ?? "",
      spotifyUris,
    );

    this.log("Updating database");
    const memberRepo = DB.getRepository(Member);
    await memberRepo.save(newMember);
  }
}
