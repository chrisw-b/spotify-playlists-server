import SpotifyWebApi from "spotify-web-api-node";
import { ONE_MIN } from "~/constants";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";

type PlaylistObjectSimplified = Awaited<
  ReturnType<SpotifyWebApi["getUserPlaylists"]>
>["body"];

export default class Playlist {
  spotifyApi: SpotifyWebApi;
  playlistName: "Most Played" | "Recently Added" | undefined;

  constructor(spotifyData: ConstructorParameters<typeof SpotifyWebApi>[0]) {
    this.spotifyApi = new SpotifyWebApi(spotifyData);
  }

  isEnabled(_: Member): boolean | undefined {
    throw new Error("Not implemented in base Playlist");
  }

  // biome-ignore lint/suspicious/useAwait: types
  async updatePlaylist(_: Member, __ = 0): Promise<void> {
    throw new Error("Not implemented in base Playlist");
  }

  log(s: string) {
    // biome-ignore lint/suspicious/noConsoleLog: logger
    console.log(`${this.playlistName ?? ""}: ${s}`);
  }

  // add list of spotify tracks to a playlist
  async fillPlaylist(
    playlistId: string | undefined,
    tracklist: { id: string }[],
  ) {
    if (!playlistId) {
      throw new Error("Unknown playlist");
    }
    return this.spotifyApi.addTracksToPlaylist(
      playlistId,
      tracklist.map((track) => track.id),
    );
  }

  async clearExistingPlaylist(
    playlist: PlaylistObjectSimplified["items"][0] | undefined,
  ) {
    // create an empty playlist
    if (playlist?.tracks.total) {
      await this.spotifyApi.removeTracksFromPlaylistByPosition(
        playlist.id,
        [...Array(playlist.tracks.total).keys()],
        playlist.snapshot_id,
      );
    }
    return playlist?.id;
  }

  findPreviousPlaylist(
    playlists: PlaylistObjectSimplified["items"],
    oldPlaylist: string | undefined,
  ) {
    this.log("searching for old playlist");
    return playlists.findIndex((p) => p.id === oldPlaylist);
  }

  async createNewPlaylist() {
    if (!this.playlistName) {
      throw new Error("Unnamed Playlist");
    }
    const newPlaylist = await this.spotifyApi.createPlaylist(
      this.playlistName,
      { public: false },
    );
    return newPlaylist.body.id;
  }

  async preparePlaylist(
    userId: string,
    oldPlaylistId: string | undefined,
    offset = 0,
  ): Promise<string | undefined> {
    const queryUserPlaylists = await this.spotifyApi.getUserPlaylists(userId, {
      limit: 20,
      offset,
    });
    const userPlaylists = queryUserPlaylists.body;
    const playlistLoc = this.findPreviousPlaylist(
      userPlaylists.items,
      oldPlaylistId,
    );
    if (playlistLoc > -1) {
      return this.clearExistingPlaylist(userPlaylists.items[playlistLoc]);
    }
    if (userPlaylists.next == null) {
      return this.createNewPlaylist();
    }
    return this.preparePlaylist(userId, oldPlaylistId, offset + 20);
  }

  async refreshToken(access: string, refresh: string) {
    // gets refresh token
    this.spotifyApi.setAccessToken(access);
    this.spotifyApi.setRefreshToken(refresh);
    const refreshedData = await this.spotifyApi.refreshAccessToken();
    return refreshedData.body;
  }

  async signInToSpotify(accessToken: string, refreshToken: string) {
    const { access_token, refresh_token } = await this.refreshToken(
      accessToken,
      refreshToken,
    );

    this.spotifyApi.setAccessToken(access_token);
    this.spotifyApi.setRefreshToken(refresh_token ?? refreshToken);
    return {
      accessToken: access_token,
      refreshToken: refresh_token ?? refreshToken,
    };
  }

  async update() {
    this.log("Starting");
    const memberRepo = DB.getRepository(Member);
    const members = await memberRepo.find({
      relations: {
        mostPlayed: this.playlistName === "Most Played",
        recentlyAdded: this.playlistName === "Recently Added",
      },
    });

    await Promise.all(
      members.map(async (member) => {
        let delayInc = 0;
        try {
          return this.isEnabled(member)
            ? this.updatePlaylist(member, delayInc++)
            : null;
        } catch (e) {
          console.error(`Error!\n${JSON.stringify(e, null, 2)}`);
          return setTimeout(() => {
            this.update().catch((reason: unknown) => {
              throw new Error(`Error updating: ${JSON.stringify(reason)}`);
            });
          }, 5 * ONE_MIN);
        }
      }),
    );
    this.log("Done!");
  }
}
