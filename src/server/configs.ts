import type LastFm from "lastfm-njs";
import type SpotifyWebApi from "spotify-web-api-node";

export const spotifyConfig: ConstructorParameters<typeof SpotifyWebApi>[0] = {
  clientId: process.env.SPOTIFY_ID,
  clientSecret: process.env.SPOTIFY_SECRET,
  redirectUri: process.env.SPOTIFY_REDIRECT,
};

export const lastfmConfig: ConstructorParameters<typeof LastFm>[0] = {
  apiKey: process.env.LASTFM_TOKEN,
  apiSecret: process.env.LASTFM_SECRET,
  username: process.env.LASTFM_USERNAME,
  password: process.env.LASTFM_PASS,
};
