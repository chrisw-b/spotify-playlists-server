import express from "express";
import type { Application } from "express-serve-static-core";
import passport from "passport";
import type { Profile } from "passport-spotify";
import { Strategy as SpotifyStrategy } from "passport-spotify";
import DB from "~/db/postgres";
import { Member } from "~/db/postgres/schema/Member";
import { MostPlayedPlaylist, Playlist } from "~/db/postgres/schema/Playlist";

const app = express.Router();

type SaveData = {
  access: string;
  refresh: string;
  userId: Profile["id"];
  photos: { value: string }[];
};

const save = async ({ access, refresh, userId, photos }: SaveData) => {
  const memberRepo = DB.getRepository(Member);

  const member = await memberRepo.findOneBy({ spotifyId: userId });
  const photo = photos.length ? photos[photos.length - 1] : { value: "" };

  if (!member) {
    const newMember = new Member();
    newMember.spotifyId = userId;
    newMember.refreshToken = refresh;
    newMember.accessToken = access;
    newMember.isAdmin = userId === process.env.ADMIN;
    newMember.photo = photo?.value ?? "";
    newMember.mostPlayed = new MostPlayedPlaylist();
    newMember.recentlyAdded = new Playlist();
    await memberRepo.save(newMember);
  } else {
    member.accessToken = access;
    member.refreshToken = refresh;
    member.photo = photo?.value ?? "";
    member.visits += 1;
    await memberRepo.save(member);
  }
};

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser<Profile>((obj, done) => {
  done(null, obj);
});

passport.use(
  new SpotifyStrategy(
    {
      clientID: process.env.SPOTIFY_ID,
      clientSecret: process.env.SPOTIFY_SECRET,
      callbackURL: process.env.SPOTIFY_REDIRECT,
    },
    (access, refresh, _, profile, done) => {
      save({
        access,
        refresh,
        userId: profile.id,
        // @ts-expect-error incorrect types
        photos: profile.photos,
      })
        .then(() => {
          done(null, { ...profile, access, refresh });
        })
        .catch((reason: unknown) => {
          throw new Error(`Error on callback: ${JSON.stringify(reason)}`);
        });
    },
  ),
);

app.get(
  "/login",
  passport.authenticate("spotify", {
    scope: process.env.SPOTIFY_SCOPES.split(","),
    successRedirect: "/",
    failureRedirect: "/",
  }) as Application<Record<string, unknown>>,
);
app.get(
  "/setup",
  passport.authenticate("spotify", {
    scope: process.env.SPOTIFY_SCOPES.split(","),
    successRedirect: process.env.CLIENT_URI,
    failureRedirect: "/",
  }) as Application<Record<string, unknown>>,
);

export default app;
