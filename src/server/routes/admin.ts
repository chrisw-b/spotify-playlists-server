import express from "express";
import { getMostPlayed, getRecentlyAdded } from "~/server/generators";

const app = express.Router();

app.post("/force-recent", (_, res) => {
  getRecentlyAdded()
    .update()
    .catch((reason: unknown) => {
      throw new Error(`Error updating recent: ${JSON.stringify(reason)}`);
    });
  res.json({ success: true });
});

app.post("/force-most", (_, res) => {
  getMostPlayed()
    .update()
    .catch((reason: unknown) => {
      throw new Error(`Error updating most played: ${JSON.stringify(reason)}`);
    });
  res.json({ success: true });
});

export default app;
