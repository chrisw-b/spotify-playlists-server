import { ONE_HOUR } from "~/constants";
import { getMostPlayed } from "~/server/generators";

const startMostPlayedTask = () => {
  const mostPlayedInterval = setInterval(() => {
    getMostPlayed()
      .update()
      .catch((reason: unknown) => {
        console.error(`MostPlayed: Error ${JSON.stringify(reason)}`);
      });
  }, 5 * ONE_HOUR);

  // wait to start intervals for 2 hours,
  // so we don't use up api running everything at once
  setTimeout(() => mostPlayedInterval, 2 * ONE_HOUR);

  // run after starting in production
  if (process.env.NODE_ENV === "production") {
    getMostPlayed()
      .update()
      .catch((reason: unknown) => {
        console.error(`MostPlayed: Error ${JSON.stringify(reason)}`);
      });
  }
};

export default startMostPlayedTask;
