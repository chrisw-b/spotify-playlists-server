import { ONE_HOUR, ONE_MIN } from "~/constants";
import { getRecentlyAdded } from "~/server/generators";

const startRecentlyAddedTask = () => {
  // run periodically
  setInterval(() => {
    getRecentlyAdded()
      .update()
      .catch((reason: unknown) => {
        console.error(`RecentlyAdded: Error ${JSON.stringify(reason)}`);
      });
  }, 5 * ONE_HOUR);

  // run after starting in production
  if (process.env.NODE_ENV === "production") {
    setTimeout(() => {
      getRecentlyAdded()
        .update()
        .catch((reason: unknown) => {
          console.error(`RecentlyAdded: Error ${JSON.stringify(reason)}`);
        });
    }, ONE_MIN * 2); // run 2 mins after most played runs
  }
};
export default startRecentlyAddedTask;
