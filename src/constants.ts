export const ONE_SEC = 1000;
export const ONE_MIN = 60 * ONE_SEC;
export const ONE_HOUR = 60 * ONE_MIN;
