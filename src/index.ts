import "reflect-metadata";

import pgSession from "connect-pg-simple";
import cookieParser from "cookie-parser";
import express from "express";
import session from "express-session";
import { createHandler } from "graphql-http/lib/use/express";
import passport from "passport";
import graphqlSchema from "~/db/graphql";
import DB from "~/db/postgres";
import adminRoute from "~/server/routes/admin";
import memberRoute from "~/server/routes/member";
import ensureAdmin from "~/server/utils/ensureAdmin";
import ensureAuthenticated from "~/server/utils/ensureAuthenticated";
import startMostPlayedTask from "~/updater/mostPlayed";
import startRecentlyAddedTask from "~/updater/recentlyAdded";

const app = express();

const PostgresqlStore = pgSession(session);
const sessionStore = new PostgresqlStore({
  conString: process.env.DB_CONNECTION_STRING,
});

app.use(cookieParser());
app.use(
  session({
    store: sessionStore,
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { secure: "auto" },
  }),
);
app.use(passport.authenticate("session"));
app.use("/member", memberRoute);
app.use("/admin", ensureAuthenticated, ensureAdmin, adminRoute);

app.use(
  "/graphql",
  ensureAuthenticated,
  createHandler({
    schema: graphqlSchema,
    context: (req) => ({
      user: req.raw.user,
      session: req.raw.session,

      logout: req.raw.logout,

      isAuthenticated: req.raw.isAuthenticated,
    }),
  }),
);

app.get("*", (_, res) => {
  res.writeHead(404);
  res.end();
});

DB.initialize()
  .then(() => {
    app.listen(process.env.PORT);

    startMostPlayedTask();
    startRecentlyAddedTask();
  })
  .catch((error: unknown) => {
    throw new Error(`Error connecting to postgres: ${JSON.stringify(error)}`);
  });
