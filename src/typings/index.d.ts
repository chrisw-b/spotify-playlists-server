import type { Profile } from "passport-spotify";

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      SPOTIFY_SCOPES: string;
      ADMIN: string;
      SPOTIFY_ID: string;
      SPOTIFY_SECRET: string;
      SPOTIFY_REDIRECT: string;
      SECRET: string;
      LASTFM_SECRET: string;
      LASTFM_TOKEN: string;
      LASTFM_USERNAME: string;
      LASTFM_PASS: string;
      DB_CONNECTION_STRING: string;
      PORT: string;
      CLIENT_URI: string;
    }
  }

  namespace Express {
    interface User extends Partial<Profile> {
      access?: string;
      refresh?: string;
    }
  }
}
